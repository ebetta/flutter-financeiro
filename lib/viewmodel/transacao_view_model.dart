// transacao_view_model.dart
import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import '../model/conta.dart';
import '../model/transacao.dart';
import '../provider/conta_list_provider.dart';
import '../provider/transacao_list_provider.dart';

class TransacaoViewModel extends ChangeNotifier {
  final TransacaoListProvider transacaoListProvider;
  final ContaListProvider contaListProvider;
  final int? filtraContaId;
  late VoidCallback _transacaoListListener;
  late VoidCallback _contaListListener;

  List<Transacao> transacoes = [];
  List<double> saldos = [];
  Conta? conta;

  bool isLoading = false;
  bool hasError = false;
  bool hasMoreData = true;
  int totalCount = 0;

  int page = 1;
  int pageSize = 20;

  final formatador = NumberFormat.currency(locale: 'pt_BR', symbol: '');

  TransacaoViewModel({
    required this.transacaoListProvider,
    required this.contaListProvider,
    required this.filtraContaId,
  }) {
    carregarTransacoes();

    // Listener de mudanças em TransacaoListProvider
    _transacaoListListener = () {
      transacoes = transacaoListProvider.transacoes;
      saldos = transacaoListProvider.saldos;

      // Atualiza conta caso tenha filtraContaId
      if (filtraContaId != null) {
        conta = contaListProvider.findByIdConta(filtraContaId!);
      }

      notifyListeners();
    };
    transacaoListProvider.addListener(_transacaoListListener);

    // Listener de mudanças em ContaListProvider
    _contaListListener = () {
      if (filtraContaId != null) {
        conta = contaListProvider.findByIdConta(filtraContaId!);
        // Sempre que a conta mudar, recalculamos os saldos a partir do saldo atual da conta
        if (conta != null) {
          transacaoListProvider.recalculaSaldos(conta!.saldoAtual);
        }
      }
      notifyListeners();
    };
    contaListProvider.addListener(_contaListListener);
  }

  Future<void> carregarTransacoes() async {
    isLoading = true;
    notifyListeners();

    try {
      transacaoListProvider.setPage(page);
      transacaoListProvider.setPageSize(pageSize);
      await transacaoListProvider.consultaTransacao(filtraContaId);

      if (filtraContaId != null) {
        conta = contaListProvider.findByIdConta(filtraContaId!);
      }

      double saldoInicial;
      if (filtraContaId == null) {
        // Calcula o patrimônio líquido quando filtraContaId é null
        saldoInicial = await calcularPatrimonioLiquido();
      } else {
        saldoInicial = conta?.saldoAtual ?? 0.0;
      }

      // Seta o saldo inicial no TransacaoListProvider
      transacaoListProvider.recalculaSaldos(saldoInicial);

      transacoes = transacaoListProvider.transacoes;
      saldos = transacaoListProvider.saldos;
      totalCount = transacaoListProvider.totalCount;

      int totalPages = (totalCount / pageSize).ceil();
      hasMoreData = page < totalPages;

      isLoading = false;
      hasError = false;
      notifyListeners();
    } catch (e) {
      isLoading = false;
      hasError = true;
      notifyListeners();
    }
  }

  Future<double> calcularPatrimonioLiquido() async {
    await contaListProvider.carregarContas();
    List<Conta> contas = contaListProvider.contas;
    double patrimonio = contas.fold(0.0, (total, conta) => total + conta.saldoAtual);
    return patrimonio;
  }

  Future<void> loadNextPage() async {
    if (hasMoreData) {
      page += 1;
      await carregarTransacoes();
    }
  }

  Future<void> loadPreviousPage() async {
    if (page > 1) {
      page -= 1;
      await carregarTransacoes();
    }
  }

  String get tituloAppBar {
    if (filtraContaId == null) {
      final patrimonioFormatted = formatador.format(patrimonioLiquido);
      return "Patrimônio  Saldo: $patrimonioFormatted";
    } else if (conta != null) {
      return "Transações - ${conta!.descricao}       Saldo: ${formatador.format(conta!.saldoAtual)}";
    } else {
      return "Transações";
    }
  }

  double get patrimonioLiquido {
    return saldos.isNotEmpty ? saldos.first : 0.0;
  }

  Conta? getContaById(int contaId) {
    return contaListProvider.findByIdConta(contaId);
  }

  @override
  void dispose() {
    transacaoListProvider.removeListener(_transacaoListListener);
    contaListProvider.removeListener(_contaListListener);
    super.dispose();
  }
}
