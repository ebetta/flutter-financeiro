// home_view_model.dart
import 'package:flutter/material.dart';
import '../model/conta.dart';
import '../provider/conta_list_provider.dart';

class HomeViewModel extends ChangeNotifier {
  final ContaListProvider contaListProvider;

  HomeViewModel({required this.contaListProvider}) {
    // Adiciona um listener ao ContaListProvider
    contaListProvider.addListener(_onContaListProviderChanged);
  }

  void _onContaListProviderChanged() {
    // Notifica os ouvintes quando o ContaListProvider mudar
    notifyListeners();
  }

  double get patrimonioLiquido {
    return contaListProvider.contas.fold(0, (sum, conta) => sum + conta.saldoAtual);
  }

  Map<int, List<Conta>> get groupedContas {
    return _groupBy(contaListProvider.contas, (Conta c) => c.tipoContaId);
  }

  Map<K, List<T>> _groupBy<T, K>(List<T> values, K Function(T) keyFunction) {
    var map = <K, List<T>>{};
    for (var element in values) {
      final key = keyFunction(element);
      map.putIfAbsent(key, () => []).add(element);
    }
    return map;
  }

  @override
  void dispose() {
    // Remove o listener para evitar vazamentos de memória
    contaListProvider.removeListener(_onContaListProviderChanged);
    super.dispose();
  }
}
