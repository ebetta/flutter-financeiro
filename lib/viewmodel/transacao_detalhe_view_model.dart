// transacao_detalhe_view_model.dart
import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import '../auth/auth_email.dart';
import '../control/regra_valor_banco.dart';
import '../model/conta.dart';
import '../model/ent_sai.dart';
import '../model/tag.dart';
import '../model/transacao.dart';
import '../provider/conta_list_provider.dart';
import '../provider/transacao_list_provider.dart';
import '../supabase/entsai_dao.dart';
import '../supabase/tag_dao.dart';
import '../supabase/transacao_dao.dart';
import '../util/conversores.dart';
import '../util/pix_input_formatter.dart';

class TransacaoDetalheViewModel extends ChangeNotifier {
  final TransacaoListProvider transacaoListProvider;
  final ContaListProvider contaListProvider;
  Transacao transacao;
  final int? filtraContaId;

  // Controladores de texto do formulário
  final descricaoController = TextEditingController();
  final valorController = TextEditingController();
  final dataMovController = TextEditingController();
  TextEditingController tagAutocompleteController = TextEditingController();

  int selectedEntSaiId = 1;
  int selectedTagId = 1;
  int? selectedContaId;
  int selectedContaTransferId = -1;

  List<EntSai> entSaiList = [];
  List<Tag> tagList = [];
  List<Conta> contaList = [];

  bool isLoading = true;
  bool hasError = false;
  String? errorMessage;

  final formatador = NumberFormat.currency(locale: 'pt_BR', symbol: '');

  TransacaoDetalheViewModel({
    required this.transacaoListProvider,
    required this.contaListProvider,
    required this.transacao,
    this.filtraContaId,
  }) {
    init();
  }

  Future<void> init() async {
    try {
      // Inicializa campos do formulário com base na transação existente
      descricaoController.text = transacao.descricao ?? '';
      valorController.text = formatador.format(
        fromBancoToAppValor(transacao.entSaiId, transacao.valor),
      );
      dataMovController.text =
          DateFormat('dd/MM/yyyy').format(transacao.dataMov ?? DateTime.now());

      selectedEntSaiId = transacao.entSaiId;
      selectedTagId = transacao.tagId;
      selectedContaId = filtraContaId ?? transacao.contaId;

      await carregarEntSai();
      await carregarConta();
      await carregarTag();

      if (contaList.isNotEmpty && selectedContaTransferId == -1) {
        selectedContaTransferId = contaList.first.id!;
      }

      isLoading = false;
      notifyListeners();
    } catch (e) {
      hasError = true;
      errorMessage = 'Erro ao carregar dados: $e';
      isLoading = false;
      notifyListeners();
    }
  }

  Future<void> carregarEntSai() async {
    entSaiList = await consultarEntSai();
  }

  Future<void> carregarTag() async {
    tagList = await consultarTagConcatenada();
    // Se já existe transação (edição), atualiza o campo de tag
    if (transacao.id != null && transacao.id != 0) {
      Tag? tagSelecionada = tagList.firstWhere(
            (tag) => tag.id == selectedTagId,
        orElse: () => tagList.first,
      );
      if (tagSelecionada != null) {
        tagAutocompleteController.text = tagSelecionada.descricao!;
      }
    }
  }

  Future<void> carregarConta() async {
    contaList = contaListProvider.contas;
  }

  Future<void> selecionarData(BuildContext context) async {
    final DateTime? picked = await showDatePicker(
      context: context,
      initialDate: transacao.dataMov ?? DateTime.now(),
      firstDate: DateTime(2000),
      lastDate: DateTime(2025),
    );
    if (picked != null) {
      dataMovController.text = DateFormat('dd/MM/yyyy').format(picked);
      notifyListeners();
    }
  }

  // Validações de campo
  String? validateDescricao(String? value) {
    if (value == null || value.isEmpty) {
      return 'Insira uma descrição';
    }
    return null;
  }

  String? validateValor(String? value) {
    if (value == null || value.isEmpty) {
      return 'Insira um valor válido';
    }
    try {
      double.parse(stringPixToDoubleString(value));
    } catch (e) {
      return 'Insira um número válido';
    }
    return null;
  }

  String? validateDataMov(String? value) {
    if (value == null || value.isEmpty) {
      return 'Insira uma data válida';
    }
    return null;
  }

  String? validateEntSaiId(int? value) {
    if (value == null) {
      return 'Por favor, selecione uma opção';
    }
    return null;
  }

  String? validateContaId(int? value) {
    if (value == null) {
      return 'Por favor, selecione uma opção';
    }
    return null;
  }

  DateTime? parseDateFromString(String dateString) {
    try {
      final DateFormat format = DateFormat('dd/MM/yyyy');
      return format.parse(dateString);
    } catch (e) {
      return null;
    }
  }

  Future<bool> inserir() async {
    if (selectedEntSaiId != 3 && tagAutocompleteController.text.isEmpty) {
      errorMessage = 'Preencha a tag.';
      notifyListeners();
      return false;
    }

    DateTime? parsedDate = parseDateFromString(dataMovController.text);
    if (parsedDate == null) {
      errorMessage = 'Erro na Data de Movimentação';
      notifyListeners();
      return false;
    }

    if (selectedEntSaiId == 3) {
      // É uma transferência
      return await inserirTransferencia();
    }

    Transacao transacaoRequest = Transacao(
      id: null,
      descricao: descricaoController.text,
      dataMov: parsedDate,
      valor: double.parse(stringPixToDoubleString(valorController.text)),
      entSaiId: selectedEntSaiId,
      tagId: selectedTagId,
      contaId: selectedContaId,
      moeda: null,
      uidFirebase: uidUsuarioLogadoFirebase(),
      transferCircularId: null,
    );

    Transacao? transacaoResponse = await insertTransacao(transacaoRequest);

    if (transacaoResponse == null) {
      errorMessage = 'Erro INSERINDO Transação';
      notifyListeners();
      return false;
    } else {
      await transacaoListProvider.consultaTransacao(transacaoResponse.contaId);
      Conta? conta = contaListProvider.findByIdConta(transacaoResponse.contaId!);
      if (conta != null) {
        Conta contaAtualizada =
        conta.copyWith(novoSaldo: conta.saldoAtual + transacaoResponse.valor);
        contaListProvider.upsertConta(contaAtualizada);
        transacao = transacaoResponse;
        return true;
      }
    }

    return false;
  }

  Future<bool> alterar() async {
    if (selectedEntSaiId != 3 && tagAutocompleteController.text.isEmpty) {
      errorMessage = 'Preencha a tag.';
      notifyListeners();
      return false;
    }

    DateTime? parsedDate = parseDateFromString(dataMovController.text);
    if (parsedDate == null) {
      errorMessage = 'Erro na Data de Movimentação';
      notifyListeners();
      return false;
    }

    Transacao transacaoRequest = Transacao(
      id: transacao.id,
      descricao: descricaoController.text,
      dataMov: parsedDate,
      valor: double.parse(stringPixToDoubleString(valorController.text)),
      entSaiId: selectedEntSaiId,
      tagId: selectedTagId,
      contaId: selectedContaId,
      moeda: null,
      uidFirebase: uidUsuarioLogadoFirebase(),
      transferCircularId: null,
    );

    Transacao? transacaoResponse = await updateTransacao(transacaoRequest);

    if (transacaoResponse == null) {
      errorMessage = 'Erro ALTERANDO Transação';
      notifyListeners();
      return false;
    } else {
      transacaoListProvider.atualizarTransacao(transacaoRequest);

      if (transacao.contaId != transacaoResponse.contaId) {
        // Conta mudou
        Conta? contaOld = contaListProvider.findByIdConta(transacao.contaId!);
        if (contaOld != null) {
          Conta contaAtualizada =
          contaOld.copyWith(novoSaldo: (contaOld.saldoAtual - transacao.valor));
          contaListProvider.upsertConta(contaAtualizada);
        }

        Conta? contaNew = contaListProvider.findByIdConta(transacaoResponse.contaId!);
        if (contaNew != null) {
          Conta contaAtualizada =
          contaNew.copyWith(novoSaldo: (contaNew.saldoAtual + transacao.valor));
          contaListProvider.upsertConta(contaAtualizada);
        }
      } else {
        // Mesma conta
        Conta? conta = contaListProvider.findByIdConta(transacaoResponse.contaId!);
        if (conta != null) {
          Conta contaAtualizada = conta.copyWith(
              novoSaldo:
              (conta.saldoAtual + (transacaoResponse.valor - transacao.valor)));
          contaListProvider.upsertConta(contaAtualizada);
        }
      }

      transacao = transacaoResponse;
      return true;
    }
  }

  Future<bool> excluir() async {
    Transacao? transacaoResponse = await excluirTransacao(transacao.id ?? -1);

    if (transacaoResponse == null) {
      errorMessage = 'Erro EXCLUINDO Transação';
      notifyListeners();
      return false;
    } else {
      transacaoListProvider.removerTransacao(transacao.id ?? -1);
      Conta? conta = contaListProvider.findByIdConta(transacaoResponse.contaId!);
      if (conta != null) {
        Conta contaAtualizada =
        conta.copyWith(novoSaldo: conta.saldoAtual - transacaoResponse.valor);
        contaListProvider.upsertConta(contaAtualizada);
      }
      return true;
    }
  }

  Future<bool> inserirTransferencia() async {
    DateTime? parsedDate = parseDateFromString(dataMovController.text);
    if (parsedDate == null) {
      errorMessage = 'Erro na Data de Movimentação';
      notifyListeners();
      return false;
    }

    if (selectedContaId == selectedContaTransferId) {
      errorMessage =
      'A conta de origem e destino não podem ser iguais para transferência';
      notifyListeners();
      return false;
    }

    double valor = double.parse(stringPixToDoubleString(valorController.text));

    Transacao transacaoSaida = Transacao(
      id: transacao.id,
      descricao: descricaoController.text,
      dataMov: parsedDate,
      valor: valor * -1,
      entSaiId: selectedEntSaiId,
      tagId: selectedTagId,
      contaId: selectedContaId,
      moeda: null,
      uidFirebase: uidUsuarioLogadoFirebase(),
      transferCircularId: null,
    );

    Transacao transacaoEntrada = Transacao(
      id: transacao.id,
      descricao: descricaoController.text,
      dataMov: parsedDate,
      valor: valor,
      entSaiId: selectedEntSaiId,
      tagId: selectedTagId,
      contaId: selectedContaTransferId,
      moeda: null,
      uidFirebase: uidUsuarioLogadoFirebase(),
      transferCircularId: null,
    );

    var respostaSaida = await insertTransacao(transacaoSaida);
    if (respostaSaida != null) {
      transacaoEntrada.transferCircularId = respostaSaida.id;
      var respostaEntrada = await insertTransacao(transacaoEntrada);
      if (respostaEntrada != null) {
        respostaSaida.transferCircularId = respostaEntrada.id;
        await updateTransacao(respostaSaida);

        await transacaoListProvider.consultaTransacao(selectedContaId);

        _atualizarSaldoContasProvider(
            respostaSaida.valor, selectedContaId, respostaEntrada.valor, selectedContaTransferId);

        return true;
      }
    }

    errorMessage = 'Erro inserindo transferência';
    notifyListeners();
    return false;
  }

  void _atualizarSaldoContasProvider(
      double valorSaida, int? contaSaidaId, double valorEntrada, int contaEntradaId) {
    var contaSaida = contaListProvider.findByIdConta(contaSaidaId!);
    if (contaSaida != null) {
      double novoSaldoSaida = contaSaida.saldoAtual + valorSaida; // valorSaida já negativo
      contaListProvider.upsertConta(contaSaida.copyWith(novoSaldo: novoSaldoSaida));
    }

    var contaEntrada = contaListProvider.findByIdConta(contaEntradaId);
    if (contaEntrada != null) {
      double novoSaldoEntrada = contaEntrada.saldoAtual + valorEntrada;
      contaListProvider.upsertConta(contaEntrada.copyWith(novoSaldo: novoSaldoEntrada));
    }
  }
}
