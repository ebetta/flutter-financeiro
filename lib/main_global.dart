import 'package:flutter/material.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  final GlobalKey<ScaffoldState> scaffoldKey = GlobalKey<ScaffoldState>();

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      home: Scaffold(
        key: scaffoldKey,
        appBar: AppBar(title: Text('Exemplo GlobalKey')),
        body: Center(
          child: ElevatedButton(
            onPressed: () {
              //scaffoldKey.currentState!.showSnackBar(
              //  SnackBar(content: Text('SnackBar ativada!')),
              //);
            },
            child: Text('Mostrar SnackBar'),
          ),
        ),
      ),
    );
  }
}
