// custom_drawer.dart
import 'package:flutter/material.dart';
import 'package:responsive_builder/responsive_builder.dart';
import '../view/conta_screen.dart';
import '../view/login_screen.dart';
import '../auth/auth_email.dart';
import '../widgets/home_content_widget.dart';

class CustomDrawer extends StatelessWidget {
  final Function(int?)? onContaSelected;

  const CustomDrawer({Key? key, this.onContaSelected}) : super(key: key);

  void _logout(BuildContext context) async {
    await signOut();
    Navigator.pushReplacement(
      context,
      MaterialPageRoute(builder: (context) => LoginScreen()),
    );
  }

  @override
  Widget build(BuildContext context) {
    return ScreenTypeLayout(
      breakpoints: ScreenBreakpoints(desktop: 600, tablet: 600, watch: 200),
      mobile: _buildMobileDrawer(context),
      desktop: _buildWebDrawer(context),
    );
  }

  Widget _buildMobileDrawer(BuildContext context) {
    return Drawer(
      child: _buildDrawerContent(context, isMobile: true),
    );
  }

  Widget _buildWebDrawer(BuildContext context) {
    return Container(
      width: 250,
      color: Colors.white,
      child: _buildDrawerContent(context, isMobile: false),
    );
  }

  Widget _buildDrawerContent(BuildContext context, {required bool isMobile}) {
    return ListView(
      padding: EdgeInsets.zero,
      children: [
        DrawerHeader(
          decoration: BoxDecoration(color: Colors.blue),
          child: Text(
            'Menu Lateral',
            style: TextStyle(color: Colors.white, fontSize: 24),
          ),
        ),
        ExpansionTile(
          leading: Icon(Icons.menu),
          title: Text('Menu'),
          initiallyExpanded: false,
          children: [
            _createDrawerItem(
              icon: Icons.home,
              text: 'Home',
              onTap: () {
                if (isMobile) Navigator.pop(context);
                // Retorna à Home sem filtro (pode ser implementado se necessário)
                Navigator.pushReplacementNamed(context, '/');
              },
            ),
            _createDrawerItem(
              icon: Icons.account_balance,
              text: 'Conta',
              onTap: () {
                if (isMobile) Navigator.pop(context);
                Navigator.push(
                  context,
                  MaterialPageRoute(builder: (context) => ContaScreen()),
                );
              },
            ),
            _createDrawerItem(
              icon: Icons.exit_to_app,
              text: 'Sair',
              onTap: () => _logout(context),
            ),
          ],
        ),
        ExpansionTile(
          leading: Icon(Icons.dashboard),
          title: Text('Resumo'),
          initiallyExpanded: true,
          children: [
            Padding(
              padding: EdgeInsets.all(16),
              child: SizedBox(
                height: 400,
                child: HomeContentWidget(
                  onContaSelected: onContaSelected,
                ),
              ),
            ),
          ],
        ),
      ],
    );
  }

  Widget _createDrawerItem(
      {IconData? icon, required String text, GestureTapCallback? onTap}) {
    return ListTile(
      leading: Icon(icon),
      title: Text(text),
      onTap: onTap,
    );
  }
}
