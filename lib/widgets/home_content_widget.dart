// home_content_widget.dart
import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'package:provider/provider.dart';
import '../viewmodel/home_view_model.dart';
import '../model/conta.dart';
import '../model/chave_valor.dart';

class HomeContentWidget extends StatelessWidget {
  final Function(int?)? onContaSelected;

  HomeContentWidget({this.onContaSelected});

  @override
  Widget build(BuildContext context) {
    return SingleChildScrollView(
      child: Column(
        children: [
          _buildPatrimonioLiquido(context),
          _buildContaList(context),
        ],
      ),
    );
  }

  Widget _buildPatrimonioLiquido(BuildContext context) {
    return Consumer<HomeViewModel>(
      builder: (context, viewModel, child) {
        final formatadorPatrimonio = NumberFormat.currency(locale: 'pt_BR', symbol: '');
        if (viewModel.patrimonioLiquido == 0 && viewModel.groupedContas.isEmpty) {
          return Center(child: CircularProgressIndicator());
        } else {
          return Align(
            alignment: Alignment.centerRight,
            child: InkWell(
              onTap: () {
                // Ao clicar no patrimônio, filtraContaId = null (todas as transações)
                if (onContaSelected != null) onContaSelected!(null);
              },
              child: Text(
                'Patrimônio: ${formatadorPatrimonio.format(viewModel.patrimonioLiquido)}',
                style: TextStyle(fontSize: 18, fontWeight: FontWeight.bold),
              ),
            ),
          );
        }
      },
    );
  }

  Widget _buildContaList(BuildContext context) {
    return Consumer<HomeViewModel>(
      builder: (context, viewModel, child) {
        final grupos = viewModel.groupedContas;

        return ListView.builder(
          shrinkWrap: true,
          physics: NeverScrollableScrollPhysics(),
          itemCount: grupos.keys.length,
          itemBuilder: (context, index) {
            final tipoContaId = grupos.keys.elementAt(index);
            final listConta = grupos[tipoContaId]!;

            return ExpansionTile(
              title: Text(mapTipoConta[tipoContaId]!),
              initiallyExpanded: true,
              children: listConta.map((conta) {
                final formatador = NumberFormat.currency(locale: 'pt_BR', symbol: '');
                return ListTile(
                  title: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      Text(conta.descricao),
                      Text(
                        formatador.format(conta.saldoAtual),
                        style: TextStyle(
                          color: conta.saldoAtual < 0 ? Colors.red : Colors.green,
                        ),
                      ),
                    ],
                  ),
                  onTap: () {
                    // Ao clicar em uma conta específica, chama o callback com o id da conta
                    if (onContaSelected != null) onContaSelected!(conta.id);
                  },
                );
              }).toList(),
            );
          },
        );
      },
    );
  }
}
