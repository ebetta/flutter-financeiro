// transacao_list_provider.dart
import 'package:flutter/foundation.dart';
import 'package:financeiro/model/transacao.dart';
import '../supabase/transacao_dao.dart';

class TransacaoListProvider with ChangeNotifier {
  List<Transacao> _transacoes = [];
  List<double> _saldos = [];
  int totalCount = 0;

  int _page = 1;
  int _pageSize = 10;
  double _saldoInicial = 0.0;

  TransacaoListProvider();

  List<Transacao> get transacoes => _transacoes;
  List<double> get saldos => _saldos;
  int get page => _page;
  int get pageSize => _pageSize;

  void setSaldoInicial(double valor) {
    _saldoInicial = valor;
    _calcularSaldos(_saldoInicial);
    notifyListeners();
  }

  void setPage(int newPage) {
    _page = newPage;
  }

  void setPageSize(int newPageSize) {
    _pageSize = newPageSize;
  }

  void _calcularSaldos(double saldoInicial) {
    // Consideramos que _transacoes está ordenado da mais recente (índice 0) para a mais antiga.
    // saldoInicial é o saldo atual da conta (após todas as transações aplicadas).
    // Logo, a primeira posição de _saldos será o saldo atual (sem remover nenhuma transação).
    double saldoAtual = saldoInicial;
    _saldos = [saldoAtual];

    // Agora, para cada transação, voltamos no tempo: removemos o valor dessa transação do saldo,
    // obtendo o saldo anterior a ela. Isso significa saldoAtual -= transacao.valor.
    // Assim, a segunda linha mostra o saldo antes da última transação, a terceira linha o saldo antes
    // da penúltima transação, e assim por diante.
    for (var transacao in _transacoes) {
      saldoAtual -= transacao.valor;
      _saldos.add(saldoAtual);
    }

    // Ao final, _saldos[0] é o saldo atual (o mesmo do título),
    // e _saldos[1] é o saldo antes da última transação, etc.
  }

  void _updateTransacoes(List<Transacao> novaLista) {
    _transacoes = novaLista;
    _calcularSaldos(_saldoInicial);
    notifyListeners();
  }

  void adicionarTransacao(Transacao transacao) {
    _transacoes.add(transacao);
    _calcularSaldos(_saldoInicial);
    notifyListeners();
  }

  void atualizarTransacao(Transacao transacao) {
    int index = _transacoes.indexWhere((c) => c.id == transacao.id);
    if (index != -1) {
      _transacoes[index] = transacao;
      _calcularSaldos(_saldoInicial);
      notifyListeners();
    }
  }

  void removerTransacao(int id) {
    _transacoes.removeWhere((transacao) => transacao.id == id);
    _calcularSaldos(_saldoInicial);
    notifyListeners();
  }

  Future<void> consultaTransacao(int? idConta) async {
    final result = await consultarTransacao(idConta, _page, _pageSize);
    _transacoes = result['data'] as List<Transacao>;
    totalCount = result['count'] as int;
    _calcularSaldos(_saldoInicial);
    notifyListeners();
  }

  void recalculaSaldos(double saldoInicial) {
    _saldoInicial = saldoInicial;
    _calcularSaldos(saldoInicial);
    notifyListeners();
  }
}
