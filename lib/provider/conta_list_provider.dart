// conta_list_provider.dart
import 'package:flutter/foundation.dart';
import '../model/conta.dart';
import '../supabase/conta_dao.dart';

class ContaListProvider with ChangeNotifier {
  List<Conta> _contas = [];

  ContaListProvider() {
    consultaConta();
  }

  List<Conta> get contas => _contas;

  Future<void> carregarContas() async {
    _contas = await consultarConta();
    notifyListeners();
  }

  Conta? findByIdConta(int id) {
    int index = _contas.indexWhere((c) => c.id == id);
    if (index != -1) {
      return _contas[index];
    }
    return null;
  }

  void adicionarConta(Conta conta) {
    _contas.add(conta);
    notifyListeners();
  }

  void upsertConta(Conta conta) {
    int index = _contas.indexWhere((c) => c.id == conta.id);
    if (index == -1) {
      _contas.add(conta);
    } else {
      _contas[index] = conta;
    }
    notifyListeners();
  }

  void removerConta(int id) {
    _contas.removeWhere((conta) => conta.id == id);
    notifyListeners();
  }

  Future<void> consultaConta() async {
    _contas = await consultarConta();
    notifyListeners();
  }
}
