// transacao_detalhe_screen.dart
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import '../model/transacao.dart';
import '../provider/conta_list_provider.dart';
import '../provider/transacao_list_provider.dart';
import '../util/alertas.dart';
import '../util/pix_input_formatter.dart';
import '../viewmodel/transacao_detalhe_view_model.dart';
import '../model/ent_sai.dart';
import '../model/tag.dart';
import '../model/conta.dart';

class TransacaoDetalheScreen extends StatelessWidget {
  final Transacao transacao;
  final int? filtraContaId;

  TransacaoDetalheScreen({required this.transacao, this.filtraContaId});

  final _formKey = GlobalKey<FormState>();

  @override
  Widget build(BuildContext context) {
    return ChangeNotifierProvider(
      create: (_) => TransacaoDetalheViewModel(
        transacaoListProvider: Provider.of<TransacaoListProvider>(context, listen: false),
        contaListProvider: Provider.of<ContaListProvider>(context, listen: false),
        transacao: transacao,
        filtraContaId: filtraContaId,
      ),
      child: Consumer<TransacaoDetalheViewModel>(
        builder: (context, viewModel, child) {
          if (viewModel.isLoading) {
            return Scaffold(
              appBar: AppBar(title: Text('Detalhe da Transação')),
              body: Center(child: CircularProgressIndicator()),
            );
          }

          if (viewModel.hasError) {
            return Scaffold(
              appBar: AppBar(title: Text('Erro')),
              body: Center(child: Text(viewModel.errorMessage ?? 'Erro desconhecido')),
            );
          }

          return Scaffold(
            appBar: AppBar(
              title: Text('Detalhe da Transação'),
              actions: [
                if (viewModel.transacao.id != 0)
                  IconButton(
                    icon: Icon(Icons.delete),
                    onPressed: () async {
                      bool result = await viewModel.excluir();
                      if (!result && viewModel.errorMessage != null) {
                        showAlerta(context, message: viewModel.errorMessage!);
                      } else if (result) {
                        Navigator.pop(context, true);
                      }
                    },
                  ),
              ],
            ),
            body: Padding(
              padding: EdgeInsets.all(16.0),
              child: Form(
                key: _formKey,
                child: ListView(
                  children: <Widget>[
                    DropdownButtonFormField<int>(
                      value: viewModel.selectedEntSaiId,
                      decoration: InputDecoration(labelText: 'Entrada/Saída'),
                      items: viewModel.entSaiList.map<DropdownMenuItem<int>>((EntSai entSai) {
                        return DropdownMenuItem<int>(
                          value: entSai.id,
                          child: Text(entSai.descricao),
                        );
                      }).toList(),
                      onChanged: (int? newValue) {
                        viewModel.selectedEntSaiId = newValue!;
                        // Apenas quando o modelo interno mudar é que chamamos notifyListeners()
                        viewModel.notifyListeners();
                      },
                      validator: (value) => viewModel.validateEntSaiId(value),
                    ),
                    TextFormField(
                      controller: viewModel.descricaoController,
                      decoration: InputDecoration(labelText: 'Descrição'),
                      validator: (value) => viewModel.validateDescricao(value),
                    ),

                    if (viewModel.selectedEntSaiId != 3)
                      Autocomplete<Tag>(
                        optionsBuilder: (TextEditingValue textEditingValue) {
                          if (textEditingValue.text.isEmpty) {
                            return const Iterable<Tag>.empty();
                          }
                          return viewModel.tagList.where((Tag tag) {
                            return tag.descricao!
                                .toLowerCase()
                                .contains(textEditingValue.text.toLowerCase());
                          });
                        },
                        displayStringForOption: (Tag tag) => tag.descricao!,
                        onSelected: (Tag selectedTag) {
                          viewModel.selectedTagId = selectedTag.id!;
                          viewModel.tagAutocompleteController.text = selectedTag.descricao!;
                          // Quando realmente selecionamos uma tag, podemos notificar
                          viewModel.notifyListeners();
                        },
                        fieldViewBuilder: (
                            BuildContext context,
                            TextEditingController fieldTextEditingController,
                            FocusNode fieldFocusNode,
                            VoidCallback onFieldSubmitted,
                            ) {
                          // Removemos a atribuição forçada do texto aqui.
                          // Assim, se for uma nova transação, o campo inicia vazio
                          // e se for edição, o ViewModel já inicializou tagAutocompleteController.
                          // Caso queira inicializar o campo apenas no caso de edição:
                          if (viewModel.transacao.id != null && viewModel.transacao.id != 0) {
                            // Se é edição e já temos uma tag selecionada, atualiza o texto inicial.
                            fieldTextEditingController.text = viewModel.tagAutocompleteController.text;
                          }

                          return TextFormField(
                            controller: fieldTextEditingController,
                            focusNode: fieldFocusNode,
                            decoration: InputDecoration(
                              labelText: 'Tag',
                              suffixIcon: fieldTextEditingController.text.isNotEmpty
                                  ? IconButton(
                                icon: Icon(Icons.clear),
                                onPressed: () {
                                  // Apenas limpa o texto do fieldTextEditingController
                                  fieldTextEditingController.clear();
                                  // Não chama notifyListeners() aqui, pois queremos
                                  // permitir que o usuário digite livremente.
                                },
                              )
                                  : null,
                            ),
                            // Não chamamos viewModel.notifyListeners() no onChanged,
                            // pois isso gera rebuild constante impedindo o Autocomplete de funcionar.
                            onChanged: (value) {
                              // Deixe o Autocomplete gerenciar o estado interno da lista.
                            },
                            onFieldSubmitted: (String value) {
                              onFieldSubmitted();
                            },
                          );
                        },
                      ),

                    TextFormField(
                      controller: viewModel.valorController,
                      decoration: InputDecoration(labelText: 'Valor'),
                      keyboardType: TextInputType.number,
                      inputFormatters: [PixInputFormatter()],
                      validator: (value) => viewModel.validateValor(value),
                    ),
                    TextFormField(
                      controller: viewModel.dataMovController,
                      decoration: InputDecoration(
                        labelText: 'Data da Movimentação',
                        suffixIcon: IconButton(
                          icon: Icon(Icons.calendar_today),
                          onPressed: () => viewModel.selecionarData(context),
                        ),
                      ),
                      keyboardType: TextInputType.datetime,
                      onTap: () => viewModel.selecionarData(context),
                      validator: (value) => viewModel.validateDataMov(value),
                    ),
                    DropdownButtonFormField<int>(
                      value: viewModel.selectedContaId,
                      decoration: InputDecoration(labelText: 'Conta'),
                      items: viewModel.contaList.map<DropdownMenuItem<int>>((Conta conta) {
                        return DropdownMenuItem<int>(
                          value: conta.id,
                          child: Text(conta.descricao),
                        );
                      }).toList(),
                      onChanged: (int? newValue) {
                        viewModel.selectedContaId = newValue;
                        viewModel.notifyListeners();
                      },
                      validator: (value) => viewModel.validateContaId(value),
                    ),
                    if (viewModel.selectedEntSaiId == 3)
                      DropdownButtonFormField<int?>(
                        value: viewModel.selectedContaTransferId != -1
                            ? viewModel.selectedContaTransferId
                            : null,
                        decoration: InputDecoration(labelText: 'Conta Transferência'),
                        items: viewModel.contaList.map<DropdownMenuItem<int>>((Conta conta) {
                          return DropdownMenuItem<int>(
                            value: conta.id,
                            child: Text(conta.descricao),
                          );
                        }).toList(),
                        onChanged: (int? newValue) {
                          viewModel.selectedContaTransferId = newValue!;
                          viewModel.notifyListeners();
                        },
                        validator: (value) {
                          if (value == null) {
                            return 'Por favor, selecione uma conta';
                          }
                          return null;
                        },
                      ),
                    SizedBox(height: 20),

                    ElevatedButton(
                      onPressed: () async {
                        if (_formKey.currentState!.validate()) {
                          bool result;
                          if (viewModel.transacao.id == null || viewModel.transacao.id == 0) {
                            result = await viewModel.inserir();
                          } else {
                            result = await viewModel.alterar();
                          }

                          if (!result && viewModel.errorMessage != null) {
                            showAlerta(context, message: viewModel.errorMessage!);
                          } else if (result) {
                            Navigator.pop(context, true);
                          }
                        }
                      },
                      child:
                      Text(viewModel.transacao.id == 0 ? 'Inserir' : 'Salvar Alterações'),
                    ),
                    SizedBox(height: 20),

                    if (viewModel.transacao.id != 0)
                      ElevatedButton(
                        onPressed: () => Navigator.pop(context),
                        child: Text('Cancelar'),
                        style: ElevatedButton.styleFrom(backgroundColor: Colors.grey),
                      ),
                  ],
                ),
              ),
            ),
          );
        },
      ),
    );
  }
}
