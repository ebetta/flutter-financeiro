import 'package:financeiro/provider/conta_list_provider.dart';
import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'package:provider/provider.dart';

import '../model/conta.dart';
import 'conta_detalhe_screen.dart';

class ContaScreen extends StatelessWidget {

  @override
  Widget build(BuildContext context) {

    final formatador = NumberFormat.currency(locale: 'pt_BR', symbol: '');

    return Scaffold(
      appBar: AppBar(title: Text("Contas")),
      body: Consumer<ContaListProvider>(
        builder: (context, storedContaListProvider, child) {
          List<Conta> listConta = storedContaListProvider.contas;
          return ListView.builder(
            itemCount: listConta.length,
            itemBuilder: (context, index) {
              Conta conta = listConta[index];
              return Card(
                shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(5), // Bordas arredondadas
                ),
                elevation: 4,
                // Pequena elevação
                margin: EdgeInsets.symmetric(horizontal: 10, vertical: 2),
                // Espaçamento
                child: ListTile(
                  title: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: <Widget>[
                      Text(conta.descricao),
                      Text(
                        formatador.format(conta.saldoAtual),
                        style: TextStyle(
                          color: conta.saldoAtual < 0 ? Colors.red : Colors
                              .green,
                        ),
                      ),
                    ],
                  ),
                  onTap: () {
                    Navigator.push(
                      context,
                      MaterialPageRoute(
                        builder: (context) => ContaDetalheScreen(conta: conta),
                      ),
                    );
                  },
                ),
              );
            },
          );
        },
      ),
      floatingActionButton: FloatingActionButton(
        onPressed: () {
          Navigator.push(
            context,
            MaterialPageRoute(
              builder: (context) => ContaDetalheScreen(
                conta: Conta.inicializa(), // Indica uma nova conta
              ),
            ),
          );
        },
        child: Icon(Icons.add),
      ),
    );
  }
}