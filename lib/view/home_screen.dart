// home_screen.dart
import 'package:flutter/material.dart';
import '../widgets/custom_drawer.dart';
import '../view/transacao_screen.dart';
import 'package:responsive_builder/responsive_builder.dart';

class HomeScreen extends StatefulWidget {
  HomeScreen({Key? key}) : super(key: key);

  @override
  _HomeScreenState createState() => _HomeScreenState();
}

class _HomeScreenState extends State<HomeScreen> {
  final GlobalKey<ScaffoldState> _scaffoldKey = GlobalKey<ScaffoldState>();
  int? _filtraContaId;

  void _onContaSelected(int? contaId) {
    setState(() {
      _filtraContaId = contaId;
    });
  }

  @override
  Widget build(BuildContext context) {
    return ResponsiveBuilder(
      builder: (context, sizingInformation) {
        bool isMobile =
            sizingInformation.deviceScreenType == DeviceScreenType.mobile;

        return Scaffold(
          key: _scaffoldKey,
          appBar: isMobile
              ? AppBar(
            title: Text('Home'),
            leading: IconButton(
              icon: Icon(Icons.menu),
              onPressed: () {
                _scaffoldKey.currentState?.openDrawer();
              },
            ),
          )
              : null,
          drawer: isMobile ? CustomDrawer(onContaSelected: _onContaSelected) : null,
          body: Row(
            children: [
              if (!isMobile) CustomDrawer(onContaSelected: _onContaSelected),
              Expanded(
                child: Padding(
                  padding: EdgeInsets.all(16),
                  child: TransacaoScreen(
                    key: ValueKey(_filtraContaId), // Garante reconstrução ao mudar filtro
                    filtraContaId: _filtraContaId,
                  ),
                ),
              ),
            ],
          ),
        );
      },
    );
  }
}
