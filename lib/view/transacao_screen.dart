// transacao_screen.dart
import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'package:provider/provider.dart';

import '../model/conta.dart';
import '../model/transacao.dart';
import '../view/transacao_detalhe_screen.dart';
import '../provider/conta_list_provider.dart';
import '../provider/transacao_list_provider.dart';
import '../viewmodel/transacao_view_model.dart';

class TransacaoScreen extends StatelessWidget {
  final int? filtraContaId;

  TransacaoScreen({
    Key? key,
    required this.filtraContaId,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return ChangeNotifierProvider(
      create: (_) => TransacaoViewModel(
        transacaoListProvider: Provider.of<TransacaoListProvider>(context, listen: false),
        contaListProvider: Provider.of<ContaListProvider>(context, listen: false),
        filtraContaId: filtraContaId,
      ),
      child: Consumer<TransacaoViewModel>(
        builder: (context, viewModel, child) {
          if (viewModel.isLoading) {
            return Center(child: CircularProgressIndicator());
          } else if (viewModel.hasError) {
            return Center(child: Text('Erro ao carregar transações.'));
          } else {
            // Dados carregados
            return Column(
              children: [
                // Título
                Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: Text(
                    viewModel.tituloAppBar,
                    style: TextStyle(fontSize: 16, fontWeight: FontWeight.bold),
                  ),
                ),
                // Lista de transações
                Expanded(
                  child: ListView.builder(
                    itemCount: viewModel.transacoes.length,
                    itemBuilder: (context, index) {
                      Transacao transacao = viewModel.transacoes[index];
                      double saldo = index < viewModel.saldos.length ? viewModel.saldos[index] : 0.0;
                      Conta? conta = viewModel.getContaById(transacao.contaId!);

                      return Card(
                        shape: RoundedRectangleBorder(
                          borderRadius: BorderRadius.circular(5),
                        ),
                        elevation: 4,
                        margin: EdgeInsets.symmetric(horizontal: 10, vertical: 2),
                        child: ListTile(
                          title: Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: <Widget>[
                              Row(
                                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                children: <Widget>[
                                  Text(DateFormat('dd/MM/yyyy').format(transacao.dataMov)),
                                  SizedBox(width: 15),
                                  Expanded(
                                    child: Text(
                                      transacao.descricao ?? '',
                                      overflow: TextOverflow.ellipsis,
                                    ),
                                  ),
                                  Text(
                                    viewModel.formatador.format(transacao.valor),
                                    style: TextStyle(
                                      color: transacao.valor < 0 ? Colors.red : Colors.green,
                                    ),
                                  ),
                                  Text(
                                    viewModel.formatador.format(saldo),
                                    style: TextStyle(
                                      color: saldo < 0 ? Colors.red : Colors.green,
                                    ),
                                  ),
                                ],
                              ),
                              Row(
                                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                children: <Widget>[
                                  Text(conta?.descricao ?? 'Conta não encontrada'),
                                ],
                              ),
                            ],
                          ),
                          onTap: () {
                            Navigator.push(
                              context,
                              MaterialPageRoute(
                                builder: (context) => TransacaoDetalheScreen(transacao: transacao),
                              ),
                            );
                          },
                        ),
                      );
                    },
                  ),
                ),
                // Rodapé com botões de paginação
                Container(
                  color: Colors.white,
                  padding: EdgeInsets.all(8.0),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      ElevatedButton(
                        onPressed: viewModel.page > 1
                            ? () async {
                          await viewModel.loadPreviousPage();
                        }
                            : null,
                        child: Text('Anterior'),
                      ),
                      SizedBox(width: 20),
                      Text('Página ${viewModel.page}'),
                      SizedBox(width: 20),
                      ElevatedButton(
                        onPressed: viewModel.hasMoreData
                            ? () async {
                          await viewModel.loadNextPage();
                        }
                            : null,
                        child: Text('Próxima'),
                      ),
                    ],
                  ),
                ),
              ],
            );
          }
        },
      ),
    );
  }
}
