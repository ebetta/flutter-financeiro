import 'package:financeiro/provider/conta_list_provider.dart';
import 'package:financeiro/supabase/conta_dao.dart';
import 'package:financeiro/util/alertas.dart';
import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'package:provider/provider.dart';
import '../auth/auth_email.dart';
import '../model/conta.dart';
import '../util/conversores.dart';
import '../util/pix_input_formatter.dart';

class ContaDetalheScreen extends StatefulWidget {
  final Conta conta;

  ContaDetalheScreen({required this.conta});

  @override
  _ContaDetalheScreenState createState() => _ContaDetalheScreenState();
}

class _ContaDetalheScreenState extends State<ContaDetalheScreen> {
  final _formKey = GlobalKey<FormState>();
  late TextEditingController _descricaoController;
  late TextEditingController _saldoInicialController;
  late TextEditingController _saldoAtualController;

  final formatador = NumberFormat.currency(locale: 'pt_BR', symbol: '');

  @override
  void initState() {
    super.initState();
    _descricaoController = TextEditingController(text: widget.conta.descricao);
    _saldoInicialController = TextEditingController(text: formatador.format(widget.conta.saldoInicial));
    _saldoAtualController = TextEditingController(text: formatador.format(widget.conta.saldoAtual));
  }

  @override
  void dispose() {
    _descricaoController.dispose();
    _saldoInicialController.dispose();
    _saldoAtualController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("Detalhe da Conta"),
        actions: [
          if (widget.conta.id != 0)
            IconButton(
              icon: Icon(Icons.delete),
              onPressed: () {
                _excluir();
              },
            ),
        ],
      ),
      body: Padding(
        padding: EdgeInsets.all(16.0),
        child: Form(
          key: _formKey,
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.stretch,
            children: <Widget>[
              TextFormField(
                controller: _descricaoController,
                decoration: InputDecoration(labelText: 'Nome da Conta'),
                validator: (value) {
                  if (value == null || value.isEmpty) {
                    return 'Insira uma descrição';
                  }
                  return null;
                },
              ),
              TextFormField(
                controller: _saldoInicialController,
                decoration: InputDecoration(labelText: 'Saldo Inicial'),
                keyboardType: TextInputType.number,
                inputFormatters: [PixInputFormatter()],
                validator: (value) {
                  if (value == null || value.isEmpty) {
                    return 'Insira o saldo inicial';
                  }
                  try {
                    double.parse(stringPixToDoubleString(value));
                  } catch (e) {
                    return 'Insira um número válido';
                  }
                  return null;
                },
              ),
              TextFormField(
                controller: _saldoAtualController,
                decoration: InputDecoration(labelText: 'Saldo Atual'),
                keyboardType: TextInputType.number,
                inputFormatters: [PixInputFormatter()],
                validator: (value) {
                  if (value == null || value.isEmpty) {
                    return 'Insira o saldo atual';
                  }
                  try {
                    double.parse(stringPixToDoubleString(value));
                  } catch (e) {
                    return 'Insira um número válido';
                  }
                  return null;
                },
              ),
              SizedBox(height: 20),
              ElevatedButton(
                onPressed: () {
                  if (_formKey.currentState!.validate()) {
                    // Chame _inserir() ou _alterar() com base no ID da conta
                    if (widget.conta.id == null || widget.conta.id == 0) {
                      _inserir();
                    } else {
                      _alterar();
                    }
                  }
                },
                child: Text(widget.conta.id == 0 ? 'Inserir' : 'Salvar Alterações'),
              ),
              if (widget.conta.id != 0)
                ElevatedButton(
                  onPressed: () => Navigator.pop(context),
                  child: Text('Cancelar'),
                  style: ElevatedButton.styleFrom(backgroundColor: Colors.grey),
                ),
            ],
          ),
        ),
      ),
    );
  }

  Conta criaConta() {
    return Conta(id: widget.conta.id,
        descricao: _descricaoController.text,
        saldoInicial: double.parse(stringPixToDoubleString(_saldoInicialController.text)),
        saldoAtual: double.parse(stringPixToDoubleString(_saldoAtualController.text)),
        uidFirebase: uidUsuarioLogadoFirebase(),
        tipoContaId: 1); //TODO alterar o 1 para pegar via DropDown
  }

  void _inserir() async {
    Conta _conta = Conta(id: null,
        descricao: _descricaoController.text,
        saldoInicial: double.parse(stringPixToDoubleString(_saldoInicialController.text)),
        saldoAtual: double.parse(stringPixToDoubleString(_saldoAtualController.text)),
        uidFirebase: uidUsuarioLogadoFirebase(),
        tipoContaId: 1); //TODO alterar o 1 para pegar via DropDown

    await upsertConta(_conta);

    Provider.of<ContaListProvider>(context, listen: false).consultaConta();
    //Provider.of<ContaListProvider>(context, listen: false).adicionarConta(_conta);

    Navigator.pop(context);
  }

  void _alterar() async {
    Conta _conta = criaConta();

    await upsertConta(_conta);

    Provider.of<ContaListProvider>(context, listen: false).upsertConta(_conta);

    Navigator.pop(context);
  }

  void _excluir() async {

    int excluiu = await excluirConta(widget.conta.id ?? -1);

    if (excluiu == -1) {
      showAlerta(context, message: 'A exclusão só ocorre quando não existe nenhuma transação nessa conta');
    } else {
      Provider.of<ContaListProvider>(context, listen: false).removerConta(widget.conta.id ?? -1);
      Navigator.pop(context);
    }
  }

}

