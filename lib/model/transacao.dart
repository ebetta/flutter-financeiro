import 'package:intl/intl.dart';

class Transacao {
  int? id;
  DateTime dataMov;
  String? descricao;
  double valor;
  int entSaiId;
  int tagId;
  int? contaId;
  int? moeda;
  String? uidFirebase;
  int? transferCircularId;

  // Construtor
  Transacao({
    this.id,
    required this.dataMov,
    this.descricao,
    required this.valor,
    required this.entSaiId,
    required this.tagId,
    required this.contaId,
    this.moeda,
    this.uidFirebase,
    this.transferCircularId,
  });

  factory Transacao.inicializa() {
    return new Transacao(
        id: null,
        dataMov: DateTime.now(), // Modificado para usar a data atual
        descricao: '',
        valor: 0.0,
        entSaiId: 1,
        tagId: 1,
        contaId: null,
        moeda: null,
        uidFirebase: null,
        transferCircularId: null,
    );
  }


  // Método para converter um objeto Transacao em um mapa JSON
  Map<String, dynamic> toJson() {
    return {
      if (id != null) 'id': id, // Inclua id apenas se não for nulo
      'data_mov': DateFormat('yyyy-MM-dd').format(dataMov),
      'descricao': descricao,
      'valor': valor,
      'ent_sai_id': entSaiId,
      'tag_id': tagId,
      'conta_id': contaId,
      'moeda': moeda,
      'uid_firebase': uidFirebase,
      'transfer_circular_id': transferCircularId,
    };
  }

  // Método para criar um objeto Transacao a partir de um mapa JSON
  factory Transacao.fromJson(Map<String, dynamic> json) {
    return Transacao(
      id: json['id'],
      dataMov: DateTime.parse(json['data_mov']),
      descricao: json['descricao'],
      valor: json['valor'].toDouble(),
      entSaiId: json['ent_sai_id'],
      tagId: json['tag_id'],
      contaId: json['conta_id'],
      moeda: json['moeda'],
      uidFirebase: json['uid_firebase'],
      transferCircularId: json['transfer_circular_id'],
    );
  }

  // Método para criar uma cópia do objeto com algumas alterações
  Transacao copyWith({
    int? id,
    required DateTime dataMov,
    String? descricao,
    required double valor,
    required int entSaiId,
    required int tagId,
    required int contaId,
    int? moeda,
    String? uidFirebase,
    int? transferCircularId,
  }) {
    return Transacao(
      id: id ?? this.id,
      dataMov: dataMov ?? this.dataMov,
      descricao: descricao ?? this.descricao,
      valor: valor ?? this.valor,
      entSaiId: entSaiId ?? this.entSaiId,
      tagId: tagId ?? this.tagId,
      contaId: contaId ?? this.contaId,
      moeda: moeda ?? this.moeda,
      uidFirebase: uidFirebase ?? this.uidFirebase,
      transferCircularId: transferCircularId ?? this.transferCircularId,
    );
  }

  @override
  String toString() {
    return 'Transacao(id: $id, dataMov: $dataMov, descricao: $descricao, valor: $valor, entSaiId: $entSaiId, tagId: $tagId, contaId: $contaId, moeda: $moeda, uidFirebase: $uidFirebase, transferCircularId: $transferCircularId)';
  }
}
