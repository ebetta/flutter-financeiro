
class Conta {
  final int? id; // Agora o id pode ser nulo
  final String descricao;
  final double saldoInicial;
  final double saldoAtual;
  final String? uidFirebase;
  final int tipoContaId;

  const Conta({
    this.id, // Remova o 'required' e deixe como opcional
    required this.descricao,
    required this.saldoInicial,
    required this.saldoAtual,
    this.uidFirebase,
    required this.tipoContaId,
  });

  factory Conta.inicializa() {
    return new Conta(descricao: '', saldoInicial: 0.0, saldoAtual: 0.0, uidFirebase: '', tipoContaId: 1);
    // Remova o id ou defina-o como null, dependendo da sua lógica de negócios
  }

  factory Conta.fromJson(Map<String, dynamic> json) {
    return Conta(
      id: json['id'] as int?, // Cast para int? caso id possa ser nulo no JSON
      descricao: json['descricao'],
      saldoInicial: json['saldo_inicial'],
      saldoAtual: json['saldo_atual'],
      uidFirebase: json['uid_firebase'] as String?,
      tipoContaId: json['tipo_conta_id'] as int,
    );
  }

  Map<String, dynamic> toJson() {
    return {
      if (id != null) 'id': id, // Inclua id apenas se não for nulo
      'descricao': descricao,
      'saldo_inicial': saldoInicial,
      'saldo_atual': saldoAtual,
      if (uidFirebase != null) 'uid_firebase': uidFirebase,
      'tipo_conta_id': tipoContaId,
    };
  }

  // Construtor nomeado para criar uma nova instância com saldo atualizado
  Conta copyWith({double? novoSaldo}) {
    return Conta(
      id: id,
      descricao: descricao,
      saldoInicial: saldoInicial,
      saldoAtual: novoSaldo ?? saldoAtual,
      uidFirebase: uidFirebase,
      tipoContaId: tipoContaId,
    );
  }
}
