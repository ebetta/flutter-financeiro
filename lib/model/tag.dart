class Tag {
  int? id;
  String? descricao;
  int? idPai;
  int? ordem;
  String? uidFirebase;

  // Construtor
  Tag({this.id, this.descricao, this.idPai, this.ordem, this.uidFirebase});

  // Método para converter um objeto Tag em um mapa JSON
  Map<String, dynamic> toJson() {
    return {
      if (id != null) 'id': id, // Inclua id apenas se não for nulo
      'descricao': descricao,
      'idPai': idPai,
      'ordem': ordem,
      'uidFirebase': uidFirebase,
    };
  }

  // Método para criar um objeto Tag a partir de um mapa JSON
  factory Tag.fromJson(Map<String, dynamic> json) {
    return Tag(
      id: json['id'],
      descricao: json['descricao'],
      idPai: json['idPai'],
      ordem: json['ordem'],
      uidFirebase: json['uidFirebase'],
    );
  }

  // Método para criar uma cópia do objeto com algumas alterações
  Tag copyWith({
    int? id,
    required String descricao,
    int? idPai,
    int? ordem,
    String? uidFirebase,
  }) {
    return Tag(
      id: id ?? this.id,
      descricao: descricao ?? this.descricao,
      idPai: idPai ?? this.idPai,
      ordem: ordem ?? this.ordem,
      uidFirebase: uidFirebase ?? this.uidFirebase,
    );
  }

  @override
  String toString() {
    return 'Tag(id: $id, descricao: $descricao, idPai: $idPai, ordem: $ordem, uidFirebase: $uidFirebase)';
  }
}
