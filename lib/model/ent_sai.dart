
import 'package:financeiro/model/generic_base_model.dart';

class EntSai implements GenericBaseModel {
  int id;
  String descricao;

  // Construtor
  EntSai({required this.id, required this.descricao});

  @override
  Map<String, dynamic> toJson() {
    return {
      'id': id,
      'descricao': descricao,
    };
  }


  factory EntSai.fromJson(Map<String, dynamic> json) {
    return EntSai(
      id: json['id'],
      descricao: json['descricao'],
    );
  }

  // Método para criar uma cópia do objeto com algumas alterações
  EntSai copyWith({
    required int id,
    required String descricao,
  }) {
    return EntSai(
      id: id ?? this.id,
      descricao: descricao ?? this.descricao,
    );
  }

  @override
  String toString() => 'EntSai(id: $id, descricao: $descricao)';
}
