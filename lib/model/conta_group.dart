import 'conta.dart';

class ContaGroup {
  int tipoContaId;
  List<Conta> contas;
  bool isExpanded; // Atributo para controlar se o painel está expandido ou não

  ContaGroup({required this.tipoContaId, required this.contas, this.isExpanded = false});
}