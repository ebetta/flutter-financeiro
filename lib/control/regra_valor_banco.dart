

//No Banco
// Se Saida o valor tem que gravar negativo
double toBancoValor(int entSaiId, double valor) {
  if (entSaiId == 2 && valor > 0) {
    return valor * -1;
  }
  return valor;
}

//Do Banco
// Se Saida o valor é negativo e tem que recuperar positivo
double fromBancoToAppValor(int entSaiId, double valor) {
  if (entSaiId == 2 && valor < 0) {
    return valor * -1;
  }
  return valor;
}