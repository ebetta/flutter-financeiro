
String stringPixToDoubleString(String brazilianFormattedString) {
  String normalizedString = brazilianFormattedString
      .replaceAll('.', '') // Remove os pontos (separadores de milhar)
      .replaceAll(',', '.'); // Substitui vírgula por ponto

  return juntaSinalNegativoComNumero(normalizedString);
}

double stringPixToDouble(String brazilianFormattedString) {
  String normalizedString = brazilianFormattedString
      .replaceAll('.', '') // Remove os pontos (separadores de milhar)
      .replaceAll(',', '.'); // Substitui vírgula por ponto

    return double.parse(normalizedString);
}

String juntaSinalNegativoComNumero(String numero) {
  if (numero.contains('-')) {
    //numero = numero.replaceAll(' ', ''); //tem caracter especial no meio
    numero = numero.substring(0,1) + numero.substring(2);
  }
  return numero;
}