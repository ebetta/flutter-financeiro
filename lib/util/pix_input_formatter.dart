
import 'package:flutter/services.dart';
import 'package:intl/intl.dart';

import 'conversores.dart';

class PixInputFormatter extends TextInputFormatter {
  @override
  TextEditingValue formatEditUpdate(TextEditingValue oldValue, TextEditingValue newValue) {
    final formatador = NumberFormat.currency(locale: 'pt_BR', symbol: '');

    //Verificar e remover temporariamente o sinal de menos
    bool isNegative = newValue.text.contains('-');
    String textEntrada = newValue.text.replaceAll('-', '').replaceAll(',', '').replaceAll('.', '');

    //Formatacao numérica
    String textDouble = textEntrada.substring(0, textEntrada.length - 2) + '.' + textEntrada.substring(textEntrada.length - 2);
    double value = double.parse(juntaSinalNegativoComNumero(textDouble));
    String newText = formatador.format(value);

    //Adicionar sinal de menos se necessário
    if (isNegative) {
      newText = '-' + newText;
    }

    return newValue.copyWith(
      text: newText,
      selection: TextSelection.collapsed(offset: newText.length),
    );
  }
}