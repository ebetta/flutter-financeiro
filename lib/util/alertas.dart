import 'package:flutter/material.dart';

import 'package:flutter/material.dart';

void showAlerta(BuildContext context, {String? title, String? message, String? buttonText}) {
  showDialog(
    context: context,
    builder: (BuildContext context) {
      return AlertDialog(
        title: Text(title ?? 'Alerta'),
        content: Text(message ?? 'Algo aconteceu!'),
        actions: <Widget>[
          TextButton(
            child: Text(buttonText ?? 'Entendi'),
            onPressed: () {
              Navigator.of(context).pop();
            },
          ),
        ],
      );
    },
  );
}

