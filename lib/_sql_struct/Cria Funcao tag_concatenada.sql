
--SELECT
SELECT t.id, COALESCE(pai.descricao || ' / ', '') || t.descricao AS descricao, t.ordem, t.uid_firebase
FROM public.tag t
LEFT JOIN public.tag pai ON t.id_pai = pai.id
WHERE t.uid_firebase = 'B3vK9H5Ah8TsxSxtJns4prkyahK2'
   OR t.id IN (
        SELECT DISTINCT id_pai
        FROM public.tag
        WHERE uid_firebase = 'B3vK9H5Ah8TsxSxtJns4prkyahK2'
        AND id_pai IS NOT NULL
    )
ORDER BY t.ordem;

--DROP
DROP FUNCTION tag_concatenada;

--CREATE FUNCTION
CREATE OR REPLACE FUNCTION tag_concatenada(uid_firebase_param text)
RETURNS TABLE(id bigint, descricao text, ordem int, uid_firebase character varying) AS $$
BEGIN
    RETURN QUERY
    SELECT t.id, COALESCE(pai.descricao || ' / ', '') || t.descricao AS descricao, t.ordem, t.uid_firebase
    FROM
        public.tag t
    LEFT JOIN
        public.tag pai ON t.id_pai = pai.id
    WHERE
        t.uid_firebase = uid_firebase_param
        OR t.id IN (
            SELECT DISTINCT
                filho.id_pai
            FROM
                public.tag filho
            WHERE
                filho.uid_firebase = uid_firebase_param
                AND filho.id_pai IS NOT NULL
        )
    ORDER BY
        t.ordem;
END;
$$ LANGUAGE plpgsql;


SELECT * FROM tag_concatenada('B3vK9H5Ah8TsxSxtJns4prkyahK2');

