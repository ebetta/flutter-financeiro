

-- Função
CREATE OR REPLACE FUNCTION update_saldo_conta()
RETURNS TRIGGER AS $$
DECLARE
    diff_value NUMERIC;
BEGIN
    IF (TG_OP = 'INSERT') THEN
        diff_value := NEW.valor;
        UPDATE conta SET saldo_atual = saldo_atual + diff_value
        WHERE id = NEW.conta_id;

    ELSIF (TG_OP = 'UPDATE') THEN
        diff_value := NEW.valor - COALESCE(OLD.valor, 0);
        IF OLD.conta_id IS NOT DISTINCT FROM NEW.conta_id THEN
            UPDATE conta SET saldo_atual = saldo_atual + diff_value
            WHERE id = NEW.conta_id;
        ELSE
            UPDATE conta SET saldo_atual = saldo_atual - OLD.valor
            WHERE id = OLD.conta_id;
            UPDATE conta SET saldo_atual = saldo_atual + NEW.valor
            WHERE id = NEW.conta_id;
        END IF;

    ELSIF (TG_OP = 'DELETE') THEN
        diff_value := -OLD.valor;
        UPDATE conta SET saldo_atual = saldo_atual + diff_value
        WHERE id = OLD.conta_id;
    END IF;

    RETURN NEW;
END;
$$ LANGUAGE plpgsql;

--Trigger
DROP TRIGGER IF EXISTS trigger_update_saldo_conta ON transacao;

CREATE TRIGGER trigger_update_saldo_conta
AFTER INSERT OR UPDATE OR DELETE ON transacao
FOR EACH ROW EXECUTE FUNCTION update_saldo_conta();
