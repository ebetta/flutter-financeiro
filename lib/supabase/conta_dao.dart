import 'dart:async';

//As Supa para não entrar em conflito com o User do Firebase
import 'package:supabase_flutter/supabase_flutter.dart';

import '../auth/auth_email.dart';
import '../model/conta.dart';


Future<List<Conta>> consultarConta() async {
    String uidFirebse = uidUsuarioLogadoFirebase();

    try {
        // Supabase
        final response = await Supabase.instance.client
            .from('conta')
            .select()
            .eq('uid_firebase', uidFirebse)
            .order('tipo_conta_id', ascending: true);

        // Mapeamento da resposta para lista de Contas
        if (response != null) {
            return response.map<Conta>((item) => Conta.fromJson(item)).toList();
        } else {
            return []; // Retorna uma lista vazia se não houver dados
        }
    } catch (e) {
        // Tratamento de exceções
        print('Erro na consulta: $e');
        return []; // Retorna uma lista vazia se não houver dados
    }
}


Future<int> upsertConta(Conta conta) async {

    //Supabase
    Map<String, dynamic> contaJson = conta.toJson();

    final supabase = Supabase.instance.client;

    final response = await supabase.from('conta').upsert(
        contaJson,
        options: const FetchOptions(count: CountOption.exact),
    );

    final count = response.count;

    return count;
}

Future<int> excluirConta(int id) async {

    try {
        final List<Map<String, dynamic>> response = await Supabase.instance.client
            .from('conta')
            .delete()
            .match({ 'id': id})
            .select();

        //return Conta.fromJson(response[0]);
        return 0;

    } catch (e) {
        print('Erro excluindo Conta, provavelmente tem Transação filha');
        return -1;
    }
}
