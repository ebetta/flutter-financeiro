
import 'package:supabase_flutter/supabase_flutter.dart';

import '../auth/auth_email.dart';
import '../model/tag.dart';

Future<List<Tag>> consultarTag() async {
  String uidFirebse = uidUsuarioLogadoFirebase();

  try {
    // Supabase
    final response = await Supabase.instance.client
        .from('tag')
        .select()
        .eq('uid_firebase', uidFirebse)
        .order('ordem', ascending: true);

    if (response != null) {
      return response.map<Tag>((item) => Tag.fromJson(item)).toList();
    } else {
      return []; // Retorna uma lista vazia se não houver dados
    }
  } catch (e) {
    // Tratamento de exceções
    print('Erro na consulta: $e');
    return []; // Retorna uma lista vazia se não houver dados
  }
}

Future<List<Tag>> consultarTagConcatenada() async {
  String uidFirebase = uidUsuarioLogadoFirebase();

  try {
    // Supabase
    final response = await Supabase.instance.client
        .rpc('tag_concatenada', params: {'uid_firebase_param': uidFirebase})
        .execute();

    if (response.data != null && response.data is List) {
      return (response.data as List)
          .map<Tag>((item) => Tag.fromJson(item))
          .toList();
    } else {
      return []; // Retorna uma lista vazia se não houver dados
    }
  } catch (e) {
    // Tratamento de exceções
    print('Erro na consulta: $e');
    return []; // Retorna uma lista vazia se houver erro
  }
}

