// transacao_dao.dart
import 'dart:async';

//As Supa para não entrar em conflito com o User do Firebase
import 'package:supabase_flutter/supabase_flutter.dart';

import '../auth/auth_email.dart';
import '../control/regra_valor_banco.dart';
import '../model/transacao.dart';


Future<Map<String, dynamic>> consultarTransacao(int? idConta, int page, int pageSize) async {
    String uidFirebase = uidUsuarioLogadoFirebase();

    try {
        int from = (page - 1) * pageSize;
        int to = from + pageSize - 1;

        var query = Supabase.instance.client
            .from('transacao')
            .select('*', FetchOptions(count: CountOption.exact))
            .eq('uid_firebase', uidFirebase);

        if (idConta != null) {
            query = query.eq('conta_id', idConta);
        }

        final response = await query
            .order('data_mov', ascending: false)
            .order('id', ascending: false)
            .range(from, to)
            .execute();

        final data = response.data as List<dynamic>;
        final totalCount = response.count ?? 0;

        final transacoes = data.map<Transacao>((item) => Transacao.fromJson(item)).toList();
        return {'data': transacoes, 'count': totalCount};
    } catch (e) {
        print('Erro na consulta: $e');
        return {'data': [], 'count': 0};
    }
}


Future<Transacao?> insertTransacao(Transacao transacao) async {

    try {
        //Regra pra gravar valor +/- no banco
        transacao.valor = toBancoValor(transacao.entSaiId, transacao.valor);

        Map<String, dynamic> transacaoJson = transacao.toJson();

        final List<Map<String, dynamic>> listData =
            await Supabase.instance.client
                .from('transacao')
                .insert(transacaoJson)
                .select();

        return Transacao.fromJson(listData[0]);

    } catch (e) {
        print('Erro inserindo Transação');
        return null;
    }
}

Future<Transacao?> updateTransacao(Transacao transacao) async {

    try {
        //Regra pra gravar valor +/- no banco
        transacao.valor = toBancoValor(transacao.entSaiId, transacao.valor);

        Map<String, dynamic> transacaoJson = transacao.toJson();

        final List<Map<String, dynamic>> listData =
            await Supabase.instance.client
                .from('transacao')
                .update(transacaoJson)
                .match({ 'id': transacao.id})
                .select();

        return Transacao.fromJson(listData[0]);

    } catch (e) {
        print('Erro atualizando Transação');
        return null;
    }
}

Future<Transacao?> excluirTransacao(int id) async {

    try {
        final List<Map<String, dynamic>> listData = await Supabase.instance.client
            .from('transacao')
            .delete()
            .match({ 'id': id})
            .select();

        return Transacao.fromJson(listData[0]);

    } catch (e) {
        print('Erro excluindo Transacao');
        return null;
    }
}
