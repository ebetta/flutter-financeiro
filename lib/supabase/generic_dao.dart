import 'package:supabase_flutter/supabase_flutter.dart';

import '../auth/auth_email.dart';
import '../model/generic_base_model.dart';

class GenericDao<T extends GenericBaseModel> {
  final String tableName;
  final String orderby;
  final T Function(Map<String, dynamic>) fromJson;

  GenericDao(this.tableName, this.orderby, this.fromJson);

  Future<List<T>> consultar() async {
    String uidFirebse = uidUsuarioLogadoFirebase();

    try {
      final response = await Supabase.instance.client
          .from(tableName)
          .select()
          .eq('uid_firebase', uidFirebse)
          .order(orderby, ascending: true);

      if (response != null) {
        return response.map<T>((item) => fromJson(item)).toList();
      } else {
        return [];
      }
    } catch (e) {
      print('Erro na consulta: $e');
      return [];
    }
  }

}