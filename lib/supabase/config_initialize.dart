import 'package:supabase_flutter/supabase_flutter.dart';

/// TODO: update with your custom SCHEME and HOSTNAME
const myAuthRedirectUri = 'io.supabase.flutterdemo://login-callback';

//Projeto Teste
//const supabaseUrl = 'https://xueixkuvqvviwmrxuyfh.supabase.co';
//const supabaseAnnonKey = 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpc3MiOiJzdXBhYmFzZSIsInJlZiI6Inh1ZWl4a3V2cXZ2aXdtcnh1eWZoIiwicm9sZSI6ImFub24iLCJpYXQiOjE2NDczNTM0OTcsImV4cCI6MTk2MjkyOTQ5N30.zyJclvyoYyV1KADtGtq1_YixaT6JLx8oqgthI34czsI';

//Projeto Orcamento
//const supabaseUrl = 'https://yfuwwhsqqwtadhkewhbo.supabase.co';
//const supabaseAnnonKey = 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpc3MiOiJzdXBhYmFzZSIsInJlZiI6InlmdXd3aHNxcXd0YWRoa2V3aGJvIiwicm9sZSI6ImFub24iLCJpYXQiOjE3MDE4OTYwMTUsImV4cCI6MjAxNzQ3MjAxNX0.GzS4Pe-h6K56TMGFuq31yd-gKsozEyWagh3rBjwzaTs';

//Orcamento Familiar
const supabaseUrl = 'https://chgcrslovtpsfomvqhsg.supabase.co';
const supabaseAnnonKey = 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpc3MiOiJzdXBhYmFzZSIsInJlZiI6ImNoZ2Nyc2xvdnRwc2ZvbXZxaHNnIiwicm9sZSI6InNlcnZpY2Vfcm9sZSIsImlhdCI6MTczMTc1NTg1NiwiZXhwIjoyMDQ3MzMxODU2fQ.QsdbuFD2oxkP4QtcENltwJuTVy8UqoZMsCWCpRGxz9s';


Future configInitSupabase() async {
  // init Supabase singleton
  // no localStorage provided, fallback to use hive as default
  await Supabase.initialize(
    url: supabaseUrl,
    anonKey: supabaseAnnonKey,
    authCallbackUrlHostname: 'login-callback',
    debug: true,
  );
}