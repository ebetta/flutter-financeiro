
import 'package:supabase_flutter/supabase_flutter.dart';

import '../model/ent_sai.dart';

Future<List<EntSai>> consultarEntSai() async {

  try {
    // Supabase
    final response = await Supabase.instance.client
        .from('ent_sai')
        .select()
        .order('id', ascending: true);

    if (response != null) {
      return response.map<EntSai>((item) => EntSai.fromJson(item)).toList();
    } else {
      return []; // Retorna uma lista vazia se não houver dados
    }
  } catch (e) {
    // Tratamento de exceções
    print('Erro na consulta: $e');
    return []; // Retorna uma lista vazia se não houver dados
  }
}