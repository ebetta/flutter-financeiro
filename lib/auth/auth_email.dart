import 'package:financeiro/auth/store_token.dart';
import 'package:flutter/material.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:shared_preferences/shared_preferences.dart';

import '../view/home_screen.dart';
import '../view/login_screen.dart';


//Display erro no final da pagina
void showErrorSnackBar(BuildContext context, String? message) {
  ScaffoldMessenger.of(context).hideCurrentSnackBar();
  ScaffoldMessenger.of(context).showSnackBar(
    SnackBar(content: Text('Error: $message')),
  );
}

//Login via e-mail e senha
Future<User?> signInWithEmail(BuildContext context, String email, String password) async {

  try {
    UserCredential userCredential = await FirebaseAuth.instance.signInWithEmailAndPassword(
        email: email, password: password
    );

    //Armazena o token
    //await storeToken(userCredential.user!.uid);

    // Armazena o estado de autenticação - principalmente por causa da WEB
    // Não funciona mesmo utilizando o FirebaseAuth.instance.setPersistence(Persistence.LOCAL);
    //Ainda nao utilizado pois teria que salvar o User
    //SharedPreferences prefs = await SharedPreferences.getInstance();
    //await prefs.setBool('isAuthenticated', true);

    // Acessando informações do usuário autenticado
    //User? user = userCredential.user;
    // print('UID: ${user?.uid}');
    // print('Email: ${user?.email}');
    // print('Nome para exibição: ${user?.displayName}');
    // print('URL da foto de perfil: ${user?.photoURL}');
    // print('Email verificado: ${user?.emailVerified}');
    // print('Usuário anônimo: ${user?.isAnonymous}');
    // print('Número de telefone: ${user?.phoneNumber}');

    return userCredential.user;

  } on FirebaseAuthException catch (e) {
    showErrorSnackBar(context, e.message);
    return null;
  }
}

//NAO FUNCIONOU
Future<User?> logaComTokenSeExistir() async {
  String? token = await getToken();
  if (token != null) {
    // Tentar autenticar o usuário usando o token existente
    try {
      UserCredential userCredential =
          await FirebaseAuth.instance.signInWithCustomToken(token);

      return userCredential.user;

    } on FirebaseAuthException catch (e) {
      return null;
    }
  }
  return null;
}

//Logout
Future signOut() async {
  //_currentJwtToken = '';
  FirebaseAuth.instance.signOut();
}


//Cria usuario via e-mail
Future<User?> createUserWithEmail(BuildContext context, String email, String password) async {

  try {
    UserCredential userCredential = await FirebaseAuth.instance.createUserWithEmailAndPassword(
      email: email,
      password: password,
    );

    User? user = userCredential.user;
    return user;

  } on FirebaseAuthException catch (e) {
    showErrorSnackBar(context, e.message);
    return null;
  }
}

//Reset senha via e-mail
Future<bool> resetPassword(BuildContext context, String email) async {
  try {
    await FirebaseAuth.instance.sendPasswordResetEmail(
      email: email,
    );
    return true;
  } on FirebaseAuthException catch (e) {
    showErrorSnackBar(context, e.message);
    return false;
  }
}

//Verifica se o Usuario está Logado e
//Redireciona para LoginScreen() ou HomeScreen()
Widget verificaSeLogado() {
  final User? user = FirebaseAuth.instance.currentUser;
  return user != null ? HomeScreen() : LoginScreen();
}

String uidUsuarioLogadoFirebase() {
  final User? user = FirebaseAuth.instance.currentUser;
  if (user != null) {
    return user.uid;
  }

  return '';
}
