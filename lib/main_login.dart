import 'package:financeiro/provider/conta_list_provider.dart';
import 'package:financeiro/provider/transacao_list_provider.dart';
import 'package:financeiro/supabase/config_initialize.dart';
import 'package:financeiro/viewmodel/home_view_model.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:firebase_core/firebase_core.dart';
import 'package:provider/provider.dart';
import 'auth/auth_email.dart';
import 'firebase_options.dart';
import 'flutter_flow/flutter_flow_theme.dart';
import 'package:flutter/foundation.dart' show kIsWeb;

void main() async {

  //Inicializa Firebase
  WidgetsFlutterBinding.ensureInitialized();

  // Inicializa o Firebase
  await Firebase.initializeApp(
    options: DefaultFirebaseOptions.currentPlatform,
  );

  //Persistência definida para LOCAL mas parece que nao funciona na WEB
  // temporariamente utilizaremos o shared_preference
  try {
    if (kIsWeb) {
      await FirebaseAuth.instance.setPersistence(Persistence.LOCAL);
    }
  } catch (e) {
    print('Erro ao definir a persistência: $e');
  }

  // // Verifica o usuário atual
  // User? currentUser = FirebaseAuth.instance.currentUser;
  // if (currentUser != null) {
  //   print('Usuário está autenticado: ${currentUser.uid}');
  // } else {
  //   print('Nenhum usuário autenticado.');
  // }

  await configInitSupabase();

  //Inicializa Flutter Flow Theme
  await FlutterFlowTheme.initialize();

  //runApp(MyApp());
  runApp(
      MultiProvider(
        providers: [
          ChangeNotifierProvider(create: (_) => TransacaoListProvider()),
          ChangeNotifierProvider(create: (_) => ContaListProvider()),
          ChangeNotifierProxyProvider<ContaListProvider, HomeViewModel>(
            create: (context) => HomeViewModel(
              contaListProvider: Provider.of<ContaListProvider>(context, listen: false),
            ),
            update: (context, contaListProvider, homeViewModel) => HomeViewModel(
              contaListProvider: contaListProvider,
            ),
          ),
        ],
        child: MyApp(),
      )
  );
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter Login Demo',
      theme: ThemeData(
        primarySwatch: Colors.blue,
      ),
      home: verificaSeLogado(),
    );
  }
}




